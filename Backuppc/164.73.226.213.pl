$Conf{RsyncClientCmd} = '$sshPath -q -x -l respaldo $host $rsyncPath $argList+';
$Conf{RsyncClientPath} = 'sudo /usr/bin/rsync';
$Conf{RsyncClientRestoreCmd} = '$sshPath -q -x -l respaldo $host $rsyncPath $argList+';
$Conf{RsyncShareName} = [
  '/etc',
  '/home',
  '/var'
];
